data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
  tags = {
    Name = "JavaApp"
    Env = "Prod"
  }
}

# EC2 instance
resource "aws_instance" "main" {
  ami                    = "ami-04d29b6f966df1537"
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.dmz.id
  vpc_security_group_ids = [aws_security_group.base.id]
  private_ip             = var.main_instance_private_ip
  tags = {
    Name = "JavaApp"
    Env = "Prod"
  }
}

resource "aws_security_group" "base" {
  name        = "Base SG"

  # Outbound HTTPS
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound HTTP
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow inbound SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }

  vpc_id = aws_vpc.main.id
  tags = {
    Name = "JavaApp"
    Env = "Prod"
  }

}

# Elastic IP
resource "aws_eip" "main_eip" {
  vpc = true
  tags = {
    Name = "JavaApp"
    Env = "Prod"
  }
}

resource "aws_s3_bucket" "lee-bucket-file" {
    bucket = "lee-bucket-file"
    acl = "private"
    lifecycle {
       prevent_destroy = false
    }
  }

resource "aws_dynamodb_table" "dynamo" {
    name = "dynamodb"
    billing_mode = "PAY_PER_REQUEST"
    hash_key       = "LockID"
    stream_enabled   = true
    stream_view_type = "NEW_AND_OLD_IMAGES"
    read_capacity  = 10
    write_capacity = 10

   attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_eip_association" "main_eip_assoc" {
  instance_id   = aws_instance.main.id
  allocation_id = aws_eip.main_eip.id
}

resource "aws_instance" "my-first-server"{
    ami           = "ami-0947d2ba12ee1ff75"
    count = 3
    instance_type = "t2.micro"
    key_name = "dockerkp"
    
    tags = {
    Name = "DBInstance"
    }
 }
