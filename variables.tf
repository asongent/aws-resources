variable "name" {
    default = "fredtech"
}

variable "region" {
    default = "us-east-1"
}

variable "AWS_ACCESS_KEY_ID" {
}

variable "AWS_SECRET_ACCESS_KEY" {
}

variable "az" {
    default = "us-east-1b"
}

variable "ssh_key_name" {
    default = "dockerkp"
}

variable "vpc_cidr_block" {
    default = "10.136.0.0/16"
}

variable "dmz_cidr_block" {
    default = "10.136.1.0/24"
}

variable "main_instance_private_ip" {
    default = "10.136.1.39"
}
